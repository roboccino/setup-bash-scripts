#!/bin/bash
echo "  "
for i in $(seq 0 5) ; do
	if [ -e "/dev/ttyACM$i" ] 
	then
		STATUS_UNO=$(udevadm info --query=all --name=/dev/ttyACM$i | grep -c Uno)
		STATUS_MEGA=$(udevadm info --query=all --name=/dev/ttyACM$i | grep -c Mega)
		if [ $STATUS_UNO == 1 ]
		then
			echo "Arduino Uno detected. Change device in servo_commander to /dev/ttyACM$i"
		elif [ $STATUS_MEGA == 1 ]
		then
			echo "Arduino Mega detected. Change device in motor_shield_commander to /dev/ttyACM$i"
		else
			echo "Different device detected"
		fi
		STATUS_UNO=0
		STATUS_MEGA=0
	fi
done
echo "  "
