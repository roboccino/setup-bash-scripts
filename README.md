# Setup bash scripts

This repository contains the setup scripts for [Roboccino](https://gitlab.com/roboccino/roboccino) project.

## Initial preparations

In your home directory, create a *cappuccino* directory and clone this repository into it. Additionally, create a subdirectory *cappuccino/ros_pipeline/src*, where you will clone both the *roboccino_interfaces* and *roboccino* ROS2 packages. Make sure to copy the aliases from *bashrc_aliases.txt* to *~/.bashrc* file.

## How to utilize the scripts and aliases

If needed, make changes to the scripts - you may have a different robot IP address, camera device names or Arduino device names.

### Hardware check
1. Make sure that everything is plugged in to the laptop (two cameras, two Arduinos).
2. Turn on two power supplies.
3. Turn on the robot and the water heater.
4. Make sure the cup is clean and dry.
5. Make sure the stirrer is dry.
6. Make sure the light ring is near the cup, facing the opposite direction with a "light disperser" (towel) on.

### Software 
1. Open the terminal.
2. Run the command: `check_devices`.
3. Apply suggested changes to ~/cappuccino/ros_pipeline/src/roboccino/config/parameters.yaml file.
4. Run the following command to enter the ROS2 directory: `coffee_start`.
5. Run the following command to build and initialize the ROS2 pipeline: `init_coffee`.
6. Choose beetwen the following commands to run the proper type of an experiment: `single_experiment`, `single_experiment_full`, `closed_loop` or `closed_loop_full`.

If you want to take a photo only, run `take_photo` command.