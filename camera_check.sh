#!/bin/bash
echo "  "
SIDE_CAMERA="BRIO" 
SIDE_CAM=$(v4l2-ctl --list-devices | grep $SIDE_CAMERA -C 1 | grep -o -P 'video.{0,1}')

if [ "$SIDE_CAM" != '' ]
then
	echo "Side camera detected. Change device in side_camera to /dev/$SIDE_CAM"
else
	echo "Side camera not detected. Plug the camera."
fi

BUBBLE_CAMERA="C920" 
BUBBLE_CAM=$(v4l2-ctl --list-devices | grep $BUBBLE_CAMERA -C 1 | grep -o -P 'video.{0,1}')
if [ "$BUBBLE_CAM" != '' ]
then
	echo "Bubble camera detected. Change device in bubble_camera to /dev/$BUBBLE_CAM"
else
	echo "Bubble camera not detected. Plug the camera."
fi
echo "  "
