#!/usr/bin/python3
ip = "192.168.1.20"
import rtde_control
print("   ")
try:
    rtde_c = rtde_control.RTDEControlInterface(ip)
    connected = rtde_c.isConnected()
    rtde_c.disconnect()
    if(connected):
        print("UR5 is connected! :)")
    else:
        print("UR5 is not connected - make sure it is properly plugged!")
except:
    print("UR5 is not connected - make sure it is properly plugged!")

print("   ")
